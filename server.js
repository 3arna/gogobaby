var express = require('express'),
    http = require('http'),
    path = require('path'),
    steam = require('./lib/steam/steam.js'),
    auth = require('./lib/steam/auth.js'),
    cheerio = require('cheerio'),
    app = express();
    

var curency = { '$': 0.745};


app.enable('strict routing');



app.set('port',  process.env.PORT || 3000);
app.set('view engine', 'dot');
app.engine('dot', require('express-dot').__express);
//app.use(express.methodOverride());
app.use(express.bodyParser());
app.use(express.cookieParser());
app.use(express.session({ secret: 'whatever'}));

app.use('/js' , express.static(path.join(process.cwd(), '/public/js')));
app.use('/css' , express.static(path.join(process.cwd(), '/public/css')));
app.use(app.router);

app.cache.requests=0;
app.cache.price = 0.35;

app.cache.types = {
  qualities : {
    Unusual : {color : '#8650AC', l:7},
    Elder : {color: '#476291', l:5},
    Heroic : {color : '#8650AC', l:6},
    Cursed : {color : '#8650AC', l:6},
    Genuine : {color : '#4d7455', l:7},
    Frozen : {color : '#4682B4', l:6},
    Auspicious : {color : '#32CD32', l:10}
  },
  gems : {
    Inscribed : {color : '#CF6A32', l:9},
    Corrupted: {color : '#A52A2A', l:9}
  },
  runes : {
    Autographed : {color : '#ADE55C', l:11}
  }
}


//routs

app.get('/', function(req, res){
	    
    if(req.session.steamid){
    	if(req.session.steamid == '76561198065634959' || req.session.steamid == '76561198065626987' || req.session.steamid == '76561198116713607')
        	res.redirect('/scan/0/10');
       	else if(req.session.steamid == '76561198046566579')
       		res.send('Ur not allowed to use our system');
       	else
       	    res.send('Si sistema skirta partneriam. Norint tapti partneriu susisiekit per steam');
    }  
    else
        res.send('Please login to the system <a href="/login">login</a>');
        
    //res.send('User steam id: '+req.session.steamid+' <a href="/logout">logout</a>');

});

app.get('/scan/(:page)/(:limit)', function(req, res){
  /*req.session.steamid = '76561198065634959';
  req.session.price = '0.35';*/
	
	if(req.session.steamid == '76561198065634959' || req.session.steamid == '76561198065626987' || req.session.steamid == '76561198116713607'){
	   
	    var types = app.cache.types;
	    var find = 'trash';
	    var limit = 10;
	    if(req.params.limit)
	        limit = req.params.limit;
	    if(req.session.limit)
	        limit = req.session.limit;
	        
	    if(req.session.find)
	      find = req.session.find;
	        
	    if(req.session.order)
	        order = req.session.order;
	    else
	        order = 'low';
	        
	    if(req.session.add)
	      add = req.session.add;
	     else
	      add = [];
	     
	     if(req.session.remove)
	      remove = req.session.remove;
	     else
	      remove = [];
	 
	 if(req.session.find == 'filter'){
	  method = 'filter'; filter = {add : add, remove : remove};
	 }
	 else{
	  method = 'market'; filter = req.session.find;
	 }
	
		steam.getFromSteam(filter, method, function(market, all){
		  //res.send(market);
			if(market){
  			var $ = cheerio.load(market);
  			var items = [];
  			var temp = {};
  			var price = '';
  			var index;
  			all = Math.ceil(all/100);
  			
  			$("a").each(function(i, elem) {
  				temp['href'] = $(this).attr('href').split('?')[0];
  	  			temp['img'] = $(this).find('img').attr("src");
  	  			temp['name'] = $(this).find('span.market_listing_item_name').text();
  	  			//temp['count'] = $(this).find('span.market_listing_num_listings_qty').text();
  	  			for(g in types.gems){
  	  			  index = temp['name'].indexOf(g);
  	  			  if(index != -1){
  	  			    temp['shortName'] = temp['name'].slice(types.gems[g].l);
  	  			    temp['gem'] = g;
  	  			    break;
  	  			  }
  	  			}
  	  			for(q in types.qualities){
  	  			  index = temp['name'].indexOf(q);
  	  			  if(index == 0){
  	  			    temp['quality'] = q;
  	  			    temp['shortName'] = temp['name'].slice(types.qualities[q].l);
  	  			    break;
  	  			  }
  	  			}
  	  			for(r in types.runes){
  	  			  index = temp['name'].indexOf(r);
  	  			  if(index != -1){
  	  			    temp['rune'] = r;
  	  			    temp['shortName'] = temp['name'].slice(types.runes[r].l);
  	  			    break;
  	  			  }
  	  			}
  	  			//console.log(temp);
  	  			var price = ($(this).find('div.market_listing_their_price').text()).split('$')[1].replace(/(\r\n\t|\n|\r|\t|USD)/gm,"");
  	  			temp['dollarPrice'] = price;
  	  			temp['price'] = Math.floor((price * curency['$']) * 100) / 100;
  	  			items.push(temp);
  	  			temp={};
  			});
  			
  			if(order == 'low'){
          		var newItems = items.sort(function(a, b){
          			return a.dollarPrice-b.dollarPrice;
          		});
  			} else {
  			    var newItems = items.sort(function(a, b){
          			return b.dollarPrice-a.dollarPrice;
          		});
  			}
  			
  			res.render('scan', {
  			  all : all,
  			  items : newItems,
  			  page : req.params.page,
  			  limit : limit,
  			  price : req.session.price,
  			  find : find,
  			  order : order,
  			  types : types,
  			  add : add,
  			  remove : remove
  			});
  			//res.send(market);
  			app.cache.requests++;
			} else {
			  res.send('refresh page');
			}
			//console.log('requests count => ' + app.cache.requests);
		}, req.params.page);
	}
	else
		res.redirect('/login');
});

app.post('/info', function(req, res){
    if(req.session.steamid && req.body){
        if(req.body.add && typeof req.body.add !== 'string'){
          req.session.add = req.body.add;
        } else if(typeof req.body.add === 'string') {
          req.session.add = [req.body.add];
        }
        
        if(req.body.remove && typeof req.body.remove !== 'string'){
          req.session.remove = req.body.remove;
        } else if(typeof req.body.remove === 'string') {
          req.session.remove = [req.body.remove];
        }
        
        req.session.price = req.body.price;
        req.session.find = req.body.find;
        req.session.limit = req.body.limit;
        req.session.order = req.body.order;
        res.redirect('/scan/0/'+req.session.limit);
    }
    else
        res.redirect('/error');
});

app.get('/login', function(req, res){
    if(req.session.steamid)
        res.redirect('/');
    else
        auth.redirect(req, res);
});

app.get('/data', function(req, res) {
    
    auth.steam(req, res, '/scan/0/10');
    
});

app.get('/logout', function(req, res){
    req.session.destroy();
    res.redirect('/');
});
 
function friends(req, res, callback){
	steam.getFromSteam(req, res, 'userFriends', function(friendList){
		
		var friendListnew = friendList.sort(function(a, b){
			return -a.friend_since+b.friend_since;
		});
		
		var friends = [];
		for(var i in friendListnew){
			friends[i] = friendList[i].steamid;
		}
		
		steam.getFromSteam(req, res, 'userData', function(data){
		
			var newData = [];
			for(var i in friends){
				for(var u in data){
					if(friends[i] == data[u].steamid){
						newData[i] = data[u];
					}
				}
			}
			
			callback(newData);
		}, friends);
    });
}

function inventory(id, callback){
	steam.getFromSteam(id, 'userInventory', function(userInventory, itemCount){
		
        if(itemCount != -1){
	        var arr = [];
	        var count = {};
	        var defindex = 0;
	        for(var i in userInventory){
	        	defindex = userInventory[i].defindex;
	            arr[i] = defindex;
	            count[defindex] = count[defindex] ? count[defindex]+1 : 1;
	        }
	        
	        count['all'] = itemCount;
	        steam.allItems(id, arr, function(data){
	        	callback(data, count);
	        });
        }
        else
        	callback(userInventory, itemCount);
    }, id);
}

/*function inventory(id, callback){
	steam.getFromSteam(id, 'userInventory', function(userInventory, itemCount){
        
        if(itemCount !== -1){
	        var arr = [];
	        var count = {};
	        var defindex = 0;
	        for(var i in userInventory){
	        	defindex = userInventory[i].defindex;
	            arr[i] = defindex;
	            count[defindex] = count[defindex] ? count[defindex]+1 : 1;
	        }
	        
	        count['all'] = itemCount;
	        steam.allItems(arr, function(data){
	        	callback(data, count);
	        });
        }
        else
        	callback(userInventory, itemCount);
    }, id);
}*/

http.createServer(app).listen(app.get('port'), function(){
    console.log('Express server listening on port ' + app.get('port'));
});